<!DOCTYPE html>
<meta charset="utf-8" />
<title>Happy Teams!</title>
<link rel="stylesheet" href="style.css" />

<?php
  error_reporting(E_ALL);
  ini_set('display_errors', '1');

  if (!isset($_POST['type'])) {
    exit('Formuläret måste postas!');
  }

  $filename = 'data.json';

  $obj = $_POST;
  if (!@$obj['uid']) $obj['uid'] = bin2hex(random_bytes(8));
  if (!@$obj['created']) $obj['created'] = date('c');
  $obj['updated'] = date('c');

  $json = file_get_contents($filename);
  $allObjs = json_decode($json, true);
  $allObjs = replaceOrAdd($allObjs, $obj);
  $json = json_encode($allObjs, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
  $json = str_replace('\r\n', '\n', $json);
  file_put_contents($filename, $json);

  $codeToName = [
    'immediate' => 'omedelbart händelsekort',
    'future_use' => 'spara-till-framtiden-kort',
    'permanent' => 'permanent händelsekort',
    'personal_blocker' => 'personlig blocker',
    'team_blocker' => 'gemensam blocker',
    'bug' => 'bugg',
    'feature' => 'feature',
    'role' => 'konsultroll',
    'role_boss' => 'chefsroll',
  ];

  $statusToName = [
    'not_set' => 'sparats',
    'in_progress' => 'markerats som <strong>inte klar</strong>',
    'complete' => 'markerats som <strong>klar</strong>',
    'deleted' => '<strong>raderats</strong>',
  ];

  function replaceOrAdd($objs, $obj) {
    foreach ($objs as $key => $value) {
      if ($value['uid'] === $obj['uid']) {
        $objs[$key] = array_merge($value, $obj);
        return $objs;
      }
    }

    $objs[] = $obj;
    return $objs;
  }
?>

<style>
  html {
    font-family: sans-serif;
  }
  pre {
    white-space: pre-wrap;
  }
</style>

<h1>Tack för förslaget!</h1>

<p>
  Om du inte ser några felmeddelanden på sidan så har ditt förslag på
  <strong><?php echo $codeToName[$obj['type']]; ?></strong>
  <?php echo $statusToName[$obj['status'] ?? 'not_set'] ?>.
</p>

<p>Du kan <a href="" onclick="history.go(-1); return false;">gå tillbaka</a> nu för att skicka in fler förslag.</p>

<details>
  <summary>Tekniska detaljer</summary>

  <pre><?php echo htmlspecialchars(json_encode($obj, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)); ?></pre>
</details>