# Happy Team!

Happy Team är ett samarbetsspel där alla spelare jobbar för att hinna klart en sprint innan kunden kommer till kontoret.

Spelet är avsett för 3-10 spelare.


# Innehåll

- 1 spelplan
- 40 vita kaffekoppar, 1 svart kaffekopp
- 8 skrivbord
- 8 spelpjäser
- 2 tärningar
- 3 rollkort för chefer
- 20 featurekort
- 120 händelsekort


# Init

Lägg dom vita kaffekopparna i köket.
Lägg den svarta kaffekoppen på startpositionen i sprintmätaren.
Blanda och lägg händelsekorten i mitten på spelplanen.
Dela slumpmässigt ut ett skrivbord till varje spelare.
Placera alla spelpjäser utanför kontoret, i trappan.

Om ni är upp till 3-5 spelare, utse 1 spelare till chef.
Om ni är 6-10 spelare, utse 2 spelare till chefer.
Alla andra spelare är konsulter.

Blanda rollkorten för chefer och dela ut till cheferna. Lägg oanvända kort åt sidan.
Blanda featurekorten och plocka ut 3 kort för varje spelare. Dessa kort ges till cheferna. Cheferna delar ut 1 featurekort till varje konsult.


# Spelets gång

Målet med spelet är implementera alla features och lösa alla buggar som dykt upp innan sprinten är slut och kunden kommer på besök.

En av cheferna börjar sin tur. När hen är klar, går turen till nästa person medsols. När det har gått en runda flyttas kunden upp i trappan och om kunden når hela vägen upp, så tar spelet omedelbart slut - ni har förlorat!


# En tur

Börja med att kasta tärningarna. Om dom visar lika, gör det som står på ditt rollkort.

Därefter gör du följande i vilken ordning du vill:

- (Obligatoriskt) Endast om du är på eller kommer in på kontoret: Ta ett händelsekort

- (Frivilligt) Gå upp till det antal steg som tärningarna visar (du kan endast gå vid ett tillfälle under din tur)

- (Arbete, frivilligt) Konsult: Om du är vid ditt skrivbord, jobba genom att ställa kaffekoppar från skrivbordet på den feature/bugg du har högst på agendan. När featuren/buggen är klar, ställ kaffekopparna i köket.

- (Arbete, frivilligt) Chef: Om du är vid ditt skrivbord, delegera features/buggar från ditt skrivbord till konsulters skrivbord. Du kan också delegera om en feature/bugg från en konsult till en annan, förutsatt att hen antingen har tomt i sin backlog, eller har en feature överst. Varje gång du delegerar en feature/bugg kostar det 1 kopp (som ställs i köket).

- (Arbete, frivilligt) Chef: Om du står på samma ruta som en konsult, delegera 1 feature från ditt skrivbord till den konsultens. Detta kostar 1 kopp från ditt skrivbord.

- (Frivilligt) Om du är i köket: Ta kaffekoppar att ställa på ditt skrivbord (det får finnas max 4 koppar på skrivbordet)

- (Frivilligt) Om du är vid någons skrivbord: Ge kaffe till den personen från ditt eget skrivbord.

- (Frivilligt) Spela spara-till-framtiden-kort


# Händelsekort

Under sin tur tar man ett händelsekort (om man är på kontoret).

Det finns några olika typer:

- Buggar: Avbryt eventuellt påbörjad feature/bugg. Du måste nu lösa buggen innan du kan arbeta med något annat.
- Roll: Om du är konsult, byt ut din nuvarande roll mot den du fick. Om du är chef, ge kortet till en konsult.
- Spara-till-framtiden-kort: Får spelas när du vill
- Omedelbar händelse: Gör det som står på kortet
- Personlig blocker: Avbryt eventuellt påbörjad feature/bugg. Du måste göra det som står på kortet innan du kan arbeta med något annat.
- Teamblocker: Alla i teamet måste göra det som står på kortet innan någon kan arbeta med något annat.
- Permanent effekt: Effekten beskriven på kortet kommer att påverka dig under resten av spelet.


# Utanför kontoret

Konsulter som är utanför kontoret måste hitta en parkeringsplats innan dom kan komma in. Om man får 5 eller mer vid sitt tärningsslag kommer man in och får då gå så många steg.
Chefer har reserverade parkeringsplatser och kan alltid komma in.
