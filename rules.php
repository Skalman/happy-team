<?php

if (isset($_POST['rules'])) {
  $markdown = $_POST['rules'];
  file_put_contents('rules.md', $markdown);
} else {
  $markdown = file_get_contents('rules.md');
}

$html = file_get_contents('rules.html');
$html = str_replace('%RULES%', htmlspecialchars($markdown), $html);
echo $html;
