<?php

if (isset($_POST['notes'])) {
  $markdown = $_POST['notes'];
  file_put_contents('notes.md', $markdown);
} else {
  $markdown = file_get_contents('notes.md');
}

$html = file_get_contents('notes.html');
$html = str_replace('%NOTES%', htmlspecialchars($markdown), $html);
echo $html;
