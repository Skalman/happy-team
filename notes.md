# Hur det har gått

2021-07-11

- Förlust: 4 spelare, 10/12 feats kvar (vi fick 11 buggar - kanske inte blandat?)
- Vinst: 5 spelare, 3 rundor kvar
- Förlust: 5 spelare, 2/15 feats kvar (vi fick 3 buggar)

2021-06-28

- Förlust: 4 spelare, 7/12 feats kvar
- Vinst: 4 spelare, klarade det på sista slaget (kunde vinna endast om vi slog 5-12, vilket vi gjorde)


2021-06-17

- Vinst: 3 spelare, klarade det på sista rundan
- Vinst: 3 spelare, ca 3 rundor kvar

# Beslut att fatta

- Kollektivt om "feature eller bugg": "uppgift"?
- "De" eller "dom"?
- Avsluta texter utan punkt. Om det är två meningar, så är det bara den sista som saknar punkt (?)