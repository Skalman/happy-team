let isContentStale = true;

/** @type {HTMLTemplateElement} */
let importTemplate;

const matchPrint = matchMedia("print");

init();

async function init() {
  await importHtml();
  const response = await fetch(`data.json?${Date.now()}`);
  const parsed = await response.json();

  setModalVisibility(false);
  const scrollTopBefore = document.documentElement.scrollTop;

  textarea.value = JSON.stringify(parsed, null, "  ");

  initCards(parsed);
  isContentStale = false;

  document.documentElement.scrollTop = scrollTopBefore;

  optionsForm.print.oninput = () => {
    if (optionsForm.print.checked) {
      optionsForm.sort.value = "type";
    }
  };
}

async function importHtml() {
  if (importTemplate) return;

  const response = await fetch("new-card.html");
  const text = await response.text();
  const template = document.createElement("template");
  template.innerHTML = text;

  document.body.append(...template.content.querySelectorAll("template"));

  importTemplate = template;
}

function initCards(cardObjects) {
  matchPrint.onchange = render;
  optionsForm.oninput = render;

  render();

  function render() {
    renderCards(cardObjects, matchPrint.matches || optionsForm.print.checked);
  }
}

function renderCards(cardObjects, print) {
  const statuses = [...optionsForm.status]
    .filter((x) => x.checked)
    .map((x) => x.value);

  const sort = optionsForm.sort.value;
  const sortMultiplier = sort === "updated" ? -1 : 1;

  const type = optionsForm.type.value;

  // Sorting affects the original array. This is on purpose.
  cardObjects.sort((a, b) => {
    let aVal, bVal;

    if (sort === "random") {
      return Math.random() - 0.5;
    } else if (sort === "title") {
      aVal = a.title || a.name || a.description || "";
      bVal = b.title || b.name || b.description || "";
    } else if (sort === "type") {
      aVal = a.type === "role_boss" ? "1" : a.type === "feature" ? "2" : a.type;
      bVal = b.type === "role_boss" ? "1" : b.type === "feature" ? "2" : b.type;
    } else {
      aVal = a[sort] || "";
      bVal = b[sort] || "";
    }

    const comparison = aVal < bVal ? -1 : aVal > bVal ? 1 : 0;
    return comparison * sortMultiplier;
  });

  cardObjects = cardObjects.filter(
    (x) =>
      statuses.includes(x.status || "in_progress") && (!type || x.type === type)
  );

  // Unique
  cardObjects = [...new Set(cardObjects)];

  if (optionsForm.showCopies.checked) {
    // Duplicate card objects if needed.
    cardObjects = cardObjects
      .map((cardObject) => Array(+cardObject.copies || 1).fill(cardObject))
      .flat();
  }

  const chunkSize = print ? 3 : 1000;
  const chunks = Array(Math.ceil(cardObjects.length / chunkSize))
    .fill()
    .map((_, i) =>
      cardObjects
        .slice(i * chunkSize, (i + 1) * chunkSize)
        .map(getCardHtml)
        .join("\n")
    )
    .map((x) => `<div class="chunk">${x}</div>`);

  cardParent.innerHTML = [
    `<div class="w-100 no-print">${cardObjects.length} kort</div>`,
    chunks.join("\n"),
  ].join("\n");

  function editCardWrapper() {
    editCard(JSON.parse(this.title));
  }
  for (const card of document.querySelectorAll(".card")) {
    card.onclick = editCardWrapper;
  }
}

/** @type {{[key: string]: HTMLFormElement} | undefined} */
let forms;
async function getEditForms() {
  if (!forms) {
    forms = {};

    for (const form of importTemplate.content.querySelectorAll("form")) {
      handlePreviews(form);
      form.onsubmit = () => {
        isContentStale = true;
      };
    }

    for (const input of importTemplate.content.querySelectorAll(
      "input[name=type]"
    )) {
      forms[input.value] = input.closest("form");
    }
  }

  return forms;
}

async function getEditForm(cardObject) {
  const forms = await getEditForms();
  const form = forms[cardObject.type];
  form.reset();
  for (const name in cardObject) {
    const input = form[name];
    if (
      input instanceof HTMLInputElement ||
      input instanceof HTMLTextAreaElement ||
      input instanceof RadioNodeList
    ) {
      input.value = cardObject[name];
      if (input.dispatchEvent) {
        input.dispatchEvent(new Event("input"));
      }
    }
  }

  return form;
}

function getAdminForm(cardObject) {
  /** @type {HTMLTemplateElement} */
  const adminFormTemplate = document.querySelector("#adminForm");
  const adminForm = adminFormTemplate.content
    .querySelector("form")
    .cloneNode(true);
  adminForm.uid.value = cardObject.uid;
  adminForm.type.value = cardObject.type;
  adminForm.status.value = cardObject.status || "in_progress";
  adminForm.copies.value = cardObject.copies || "1";
  adminForm.onsubmit = () => {
    isContentStale = true;
  };
  return adminForm;
}

async function editCard(cardObject) {
  modal.textContent = "";
  modal.append(getAdminForm(cardObject), await getEditForm(cardObject));
  setModalVisibility(true);
}

function setModalVisibility(visible) {
  modal.hidden = !visible;
  document.body.classList.toggle("modal-open", visible);
}

modal.onclick = (e) => {
  if (e.target === modal) {
    setModalVisibility(false);
  }
};
window.addEventListener("keydown", (e) => {
  if (e.key === "Escape") {
    setModalVisibility(false);
  }
});
window.addEventListener("focus", (e) => {
  if (isContentStale) {
    setModalVisibility(false);
    init();
  }
});
