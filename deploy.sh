#!/bin/bash

# Abort the script if a command returns an error.
set -e

if [ -f .env ]; then
  # Export variables from .env file.
  set -o allexport
  source .env
  set +o allexport
fi

# Transfer the content to the remote directory.
rsync -avz --delete \
  --delete-excluded \
  --exclude={deploy.sh,.env,.env.example,.git,.gitignore} \
  -e ssh ./ "$USERNAME@$REMOTE_HOST:$REMOTE_DIR"

