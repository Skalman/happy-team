/**
 * @param {HTMLFormElement} form
 */
function handlePreviews(form) {
  /** @type {(HTMLInputElement|HTMLTextAreaElement)[]} */
  const inputs = [...form.querySelectorAll("input, textarea")];
  const preview = form.querySelector(".preview");

  render();
  for (const input of inputs) {
    input.oninput = render;
  }

  function render() {
    const data = Object.fromEntries(
      inputs
        .map((input) => {
          if (input.type === "radio" && !input.checked) return;
          return [input.name, input.value];
        })
        .filter((x) => x)
    );

    preview.innerHTML = getCardHtml(data);
  }
}

function getCardHtml(data) {
  const template = document.getElementById("template-" + data.type);
  if (!template) return "?";

  let html = template.innerHTML;

  for (const [key, val] of [
    ...Object.entries(data),
    ["quoteWithMarks", /^\W/.test(data.quote ?? "") ? data.quote ?? "" : `\u201d${data.quote}\u201d`],
    ["json", JSON.stringify(data)],
  ]) {
    html = html.replace(
      `{{${key}}}`,
      toHtml(val)
    );
  }

  return html;
}

function toHtml(str) {
  return str
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/"/g, "&quot;")
    .replace(/(\d) /g, "$1\u00a0")
    .replace(/\u2615 /g, "\u2615\u00a0") // Kaffekopp
    .replace(/\*(.+?)\*/g, "<i>$1</i>");
}